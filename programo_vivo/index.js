const compression = require('compression');
const express = require('express');
const server = express();

//MIDDLEWARES
server.use(function(req,res,next){
    console.log(req.url);
    next();
});

server.use(function(req,res,next){
    console.log('Time: ',Date.now());
    next();
});

function validarUsuario(req,res,next){
    if(req.query.usuario != 'admin'){
        res.json("usuario inválido");
    }else{
        next();
    };
};

server.use(compression())

//ENDPOINTS
server.get('/',(req,res)=>{
    res.json('Hola Mundo')
});

server.get('/saludo_query/usuario',validarUsuario,(req,res)=>{
    let data = req.query;
    res.json('Usuario validado correctamente')
});

server.get('/probarcompression',function(req,res){
    const animal = 'canguro';
    res.send(animal.repeat(1000))
});

server.listen(3000, () => console.log("listening on 3000"));