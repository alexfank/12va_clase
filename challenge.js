const express = require('express');
const app = express();

let cursos = [{
    curso: "Diseño Web Full Stack",
    id: "DWFS"
    },{
    curso: "Diseño Web Back End",
    id: "DWBE" 
    },{
    curso: "Diseño Web Front End",
    id: "DWFE"
}
];

const logger = (req, res, next) => {
    console.log(`request HTTP method: ${req.method}`);
    next();
};

app.get('/cursos',(req,res)=>{
    res.json(cursos)
    app.use(logger)
});



app.listen(5000, () => console.log("listening on 5000"));