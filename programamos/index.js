const express = require('express');
const app = express();

app.use(express.urlencoded({extended: true}));
app.use(express.json());

//MIDDLEWARE

const autores = [{
    id: 1,
    nombre: "Jorge Luis",
    apellido: "Borges",
    fechaDeNacimiento: "24/08/1899",
    libros: [
        {
            id: 1,
            titulo: "Ficciones",
            descripcion: "Se trata de uno de sus más...",
            anioPublicacion: "1944"
        },{
            id: 2,
            titulo: "El Aleph",
            descripcion: "Otra recopilación de cuentas",
            anioPublicacion: "1949"
        }
    ]
    },{
    id: 2,
    nombre: "Ernesto",
    apellido: "Sábato",
    fechaDeNacimiento: "24/06/1911",
    libros: [
        {
            id: 1,
            titulo: "El Túnel",
            descripcion: "Se trata de uno de sus más...",
            anioPublicacion: "1948"
        },{
            id: 2,
            titulo: "Sobre Héroes y Tumbas",
            descripcion: "Una de sus novelas más conocidas...",
            anioPublicacion: "1961"
        }
    ]
}];

//MIDDLEWARES
const validarAutor = (req,res,next) => {
    let id_autor = req.params.id;
    autores.forEach(autor => {
        if(id_autor == autor.id){
            next();
        }
    });
    res.send('El ID del autor no existe.');
        
};

const validarLibro = (req,res,next) => {
    let id_libro = req.params.idLibro;
    
    autores.forEach(autor => {
        autor.libros.forEach(libro => {
            if(libro.id == id_libro){

                next();
            };
        });
});
        
        res.send('El ID del libro no existe.');
};

//CREO ENDPOINTS GET

app.get('/autores',(req,res)=>{
    
    res.json(autores)
});

app.get('/autores/:id',validarAutor,(req,res)=>{
    let id_autor = req.params.id;
    autores.forEach(autor => {
        if(autor.id == id_autor){
            res.json(autor)
        }
    });
});

app.get('/autores/:id/libros',validarAutor,(req,res)=>{
    let id_autor = req.params.id;
    autores.forEach(autor => {
        if(autor.id == id_autor){
            res.json(autor.libros)
        }
    });
});

app.get('/autores/:id/libros/:idLibro',validarLibro,(req,res)=>{
    let id_autor = req.params.id;
    let id_libro = req.params.idLibro;

    autores.forEach(autor => {
        if(id_autor == autor.id){
            autor.libros.forEach(libro => {
                if(libro.id == id_libro)
                return res.json(libro)
            });
        }

        res.send('Libro no encontrado.')

    });
});

//CREO ENDPOINTS POST

app.post('/autores/crear',(req,res)=>{
    const {id, nombre, apellido, fechaDeNacimiento, libros} = req.query;

    const nuevoAutor = {
        id: id,
        nombre: nombre,
        apellido: apellido,
        fechaDeNacimiento: fechaDeNacimiento,
        libros: libros,
    }

    autores.push(nuevoAutor);

    res.send('Autor agregado correctamente');
})

app.post('/autores/:id/libros/crear',validarAutor,(req,res)=>{
    let id_autor = req.params.id;
    const {id, titulo, descripcion, anioPublicacion} = req.query;

    const nuevoLibro = {
        id: id,
        titulo: titulo,
        descripcion: descripcion,
        anioPublicacion: anioPublicacion
    }
    let pos = autores.indexOf(id_autor);
    autores[pos].libros.push(nuevoLibro);

    res.send('Libro agregado correctamente');
})


//CREO ENDPOINTS DELETE

app.delete('/autores/:id/del',validarAutor,(req,res)=>{
    let id_autor = req.params.id;
    let pos = autores.indexOf(id_autor);

    autores.splice(pos, 1);

    res.send('Autor borrado correctamente');
})

app.delete('/autores/:id/libros/:idLibro/del',validarAutor,validarLibro,(req,res)=>{
    let id_autor = req.params.id;
    let id_libro = req.params.idLibro;
    
    let pos_autor = autores.indexOf(id_autor);
    let pos_libro = autores.indexOf(id_libro);

    autores[pos_autor].libros.splice(pos_libro, 1);

    res.send('Libro eliminado correctamente');
})

app.listen(3000, () => console.log("listening on 3000"));